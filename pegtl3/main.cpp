#include "grammar.h"

#include <iostream>

namespace perso {
using namespace grammar;

struct left:      plus<digit>{};
struct op:        plus<alpha>{};
struct right:     plus<digit>{};
struct operation: must<left, pad<op, blank>, right>{};

auto foo(std::string op) {
    if (op == "add") {
        return [](auto l, auto r) { return l + r; };
    }
    throw "Invalid";
}

}; // namespace perso

int main()
{
    using namespace perso;
    using namespace std;
    using namespace grammar;

    auto input = "123au4estr"s;
    std::string::const_iterator b = std::cbegin(input);
    std::string::const_iterator e = std::cend(input);
    try {
        for (auto it: parse<grammar::plus<digit>>(b, e)) {
            std::cout << it << std::endl;
        }
    }
    catch (parse_error& e) {
        std::cout << "error: ";
        while (e.it != e.end) {
            std::cout << *e.it++;
        }
    }

    return 0;
}
