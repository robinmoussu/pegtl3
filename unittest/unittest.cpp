// #include "catch.hpp"
// #include "grammar.h"
// 
// #include <string>
// 
// using namespace std;
// 
// TEST_CASE("Test the basic axioms", "[axiom]")
// {
//     CHECK(grammar::parse<grammar::alpha>("abc123d4") == 'a');
//     CHECK(grammar::parse<grammar::alphanum>("abc123d4") == 'a');
//     CHECK(grammar::parse<grammar::alphanum>("0abc123d4") == '0');
//     CHECK(grammar::parse<grammar::blank>(" abc123d4") == ' ');
//     CHECK(grammar::parse<grammar::digit>("0abc123d4") == 0); // return an integer, not a char
// 
//     REQUIRE_THROWS_AS(grammar::parse<grammar::alpha>("*abc123d4"), grammar::parse_error);
//     REQUIRE_THROWS_AS(grammar::parse<grammar::alphanum>("*abc123d4"), grammar::parse_error);
//     REQUIRE_THROWS_AS(grammar::parse<grammar::blank>("*abc123d4"), grammar::parse_error);
//     REQUIRE_THROWS_AS(grammar::parse<grammar::digit>("*abc123d4"), grammar::parse_error);
// }
// 
// TEST_CASE("Sequence of alphabetic characters", "[alpha]")
// {
//     CHECK(grammar::parse<grammar::plus<grammar::alpha>>("abc123d4")
//         == vector({'a', 'b', 'c'}));
// }
// 
// TEST_CASE("Sequence of alphabetic and digit characters", "[alphanum]")
// {
//     CHECK(grammar::parse<grammar::plus<grammar::alphanum>>("abc123d4 efg")
//         == vector({'a', 'b', 'c', '1', '2', '3', 'd', '4'}));
// }
// 
// TEST_CASE("Sequence of blank characters","[blank]")
// {
//     CHECK(grammar::parse<grammar::plus<grammar::blank>>(" \tabc123d4")
//         == vector({' ', '\t'}));
// }
// 
// TEST_CASE("Sequence of digits", "[digit]")
// {
//     CHECK(grammar::parse<grammar::plus<grammar::digit>>("123au4estr")
//         == vector({1, 2, 3}));
// }
// 
// TEST_CASE("Read a number", "[number]")
// {
//     CHECK(grammar::parse<grammar::number>("123au4estr") == 123);
// }
// 
// TEST_CASE("Parse a string that doesn't start with an alphabetic caracter", "[alpha]")
// {
//     REQUIRE_THROWS_AS(grammar::parse<grammar::plus<grammar::alpha>>("*abc123d4"),
//         grammar::parse_error);
// }
// 
// TEST_CASE("Parse a string that doesn't start with a blank caracter","[blank]")
// {
//     REQUIRE_THROWS_AS(grammar::parse<grammar::plus<grammar::blank>>("* \tabc123d4"),
//         grammar::parse_error);
// }
// 
// TEST_CASE("Parse a string that doesn't start with a digit", "[digit]")
// {
//     REQUIRE_THROWS_AS(grammar::parse<grammar::plus<grammar::digit>>("*123au4estr"),
//         grammar::parse_error);
// }
// 
// TEST_CASE("Parse a string", "[string]")
// {
//     CHECK(grammar::parse<grammar::string>("abc123d4 efg") == "abc123d4"s);
//     CHECK(grammar::parse<grammar::string>("abc123d4+efg") == "abc123d4"s);
// }
// 
// TEST_CASE("Parse characters", "[character]")
// {
//     CHECK(grammar::parse<grammar::character<'+'>>("+++string---else") == '+');
//     CHECK(grammar::parse<grammar::plus<grammar::character<'+'>>>("+++string---else") == vector({'+', '+', '+'}));
// }
// 
// TEST_CASE("Parse a string surrounded by padding", "[pad]")
// {
//     CHECK(grammar::parse<grammar::pad<
//             grammar::string,
//             grammar::character<'+'>,
//             grammar::character<'-'>>
//         >("+++string---else") == "string"s);
// 
//     CHECK(grammar::parse<grammar::pad<
//             grammar::digit,
//             grammar::character<'-'>>
//         >("---1---else") == 1);
// }
// 
// TEST_CASE("Parse a sequence of token", "[seq]")
// {
//     CHECK(grammar::parse<grammar::seq<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::string,
//             grammar::plus<grammar::character<'-'>>>
//         >("+++string---else")
//         == tuple{
//             vector({'+', '+', '+'}),
//             "string"s,
//             vector({'-', '-', '-'})
//         });
// 
//     struct my_struct {
//         vector<char> left;
//         int t;
//         vector<char> right;
//         string word;
// 
//         bool operator == (const my_struct& other) const {
//             return this->t == other.t && this->left == other.left && this->right == other.right;
//         };
//     };
// 
//     CHECK(util::to_struct<my_struct>::get(grammar::parse<grammar::seq<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::number,
//             grammar::plus<grammar::character<'-'>>,
//             grammar::string>
//         >("+++1234---word"))
//         == my_struct{
//             vector({'+', '+', '+'}),
//             1234,
//             vector({'-', '-', '-'}),
//             "else"s
//         });
// }
// 
// TEST_CASE("Parse an altenate sequence of token", "[sor]")
// {
//     // NB: the std::get will just select the correct variant to be able to call
//     // operator==. It has nothing to do with the parsing.
//     CHECK(std::get<std::vector<char>>(
//         grammar::parse<grammar::sor<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::string,
//             grammar::number,
//             grammar::plus<grammar::character<'-'>>>
//         >("+++1234---else"))
//         == vector({'+', '+', '+'}));
// 
//     CHECK(std::get<int>(
//         grammar::parse<grammar::sor<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::number,
//             grammar::string,
//             grammar::plus<grammar::character<'-'>>>
//         >("1234---else"))
//         == 1234);
// 
//     CHECK(std::get<std::vector<char>>(
//         grammar::parse<grammar::sor<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::string,
//             grammar::number,
//             grammar::plus<grammar::character<'-'>>>
//         >("---else"))
//         == vector({'-', '-', '-'}));
// 
//     CHECK(std::get<std::string>(
//         grammar::parse<grammar::sor<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::string,
//             grammar::number,
//             grammar::plus<grammar::character<'-'>>>
//         >("else"))
//         == "else"s);
// 
//     auto no_comma_for_macro = [](auto str) {
//         grammar::parse<grammar::sor<
//             grammar::plus<grammar::character<'+'>>,
//             grammar::string,
//             grammar::number,
//             grammar::plus<grammar::character<'-'>>>
//         >(str);
//     };
//     REQUIRE_THROWS_AS(
//         no_comma_for_macro("####+++1234---else"),
//         grammar::parse_error);
// }
// 
