#include "catch.hpp"
#include "grammar.h"
#include "utils.h"

#include <string>

namespace grammar {

// struct operation;
// struct operande: sor<pad<operation, character<'('>, character<')'>>, number>{};
// struct addition: seq<operande, character<'+'>, operande> {};
// struct substraction: seq<operande, character<'-'>, operande> {};
// struct operation: sor<addition, substraction> {};

// struct addition: plus<alphanum>{};
// struct substraction: plus<digit>{};
// struct operation: sor<addition, substraction> {};

// template<>
// inline auto parse<grammar::addition>(std::string::const_iterator& it, std::string::const_iterator end) {
//     return default_parse_as<grammar::addition, std::string>(it, end);
// }
// 
// template<>
// inline auto parse<grammar::substraction>(std::string::const_iterator& it, std::string::const_iterator end) {
//     struct number_base_10 {
//         int value;
//         void push_back(int i) {
//             this->value *= 10;
//             this->value += i;
//         }
//     };
//     return default_parse_as<grammar::substraction, number_base_10>(it, end).value;
// }

// template<>
// inline auto parse<grammar::operation>(std::string::const_iterator& it, std::string::const_iterator end) {
//     struct number_base_10 {
//         int value;
//         void push_back(int i) {
//             this->value *= 10;
//             this->value += i;
//         }
//     };
//     return default_parse_as<grammar::operation, number_base_10>(it, end).value;
// }

// struct operation;
// struct operande: sor<pad<operation, character<'('>, character<')'>>, number>{};
// struct addition: seq<operande, character<'+'>, operande> {};
// struct substraction: seq<operande, character<'-'>, operande> {};
// struct operation: sor<addition, substraction> {};

struct operande: number{};
struct addition: seq<operande, character<'+'>, operande> {};
struct substraction: seq<operande, character<'-'>, operande> {};
struct operation: sor<addition, substraction> {};

template<class T>
struct computation{
    T left;
    char op;
    T right;
};

inline int parse(const grammar::addition& axiom, std::string::const_iterator& it, std::string::const_iterator end) {
    static_assert(std::is_same<std::vector<decltype(parse(util::get_instance<grammar::number>::value, it, end))>, std::vector<int>>::value);
    std::tuple<int, char, int> data = default_parse(axiom, it, end);
    static_assert(std::is_same<decltype(default_parse(util::get_instance<digit>::value, it, end)), int>::value);
    static_assert(std::is_same<decltype(parse(util::get_instance<number>::value, it, end)), int>::value);
    static_assert(std::is_same<decltype(default_parse(util::get_instance<grammar::string>::value, it, end)), std::vector<char>>::value);

    static_assert(std::is_same<decltype(default_parse(util::get_instance<addition>::value, it, end)), std::tuple<int,char,int>>::value);
    static_assert(std::is_same<decltype(default_parse(util::get_instance<seq<operande, character<'+'>, operande>>::value, it, end)), std::tuple<int,char,int>>::value);
    static_assert(std::is_same<decltype(data), std::tuple<int,char,int>>::value);
    auto ret = util::to_struct<computation<int>>::get(data);
    return ret.left + ret.right;
}

// auto parse<grammar::substraction>(std::string::const_iterator& it, std::string::const_iterator end) {
//     auto data = default_parse(axiom, it, end);
//     auto ret = util::to_struct<computation<int>>::get(data);
//     return ret.left - ret.right;
// }


};

// TEST_CASE("compute addition and substraction with parenthesis", "[full]")
// {
//     CHECK(grammar::parse<grammar::addition>("23+2-(15+(5-10))+100") == 114);
// }
