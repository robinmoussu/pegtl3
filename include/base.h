#pragma once

#include <string>

namespace grammar {

/* Each axiom must provide the default_parse() function.
 *
 * Each aggregate (types created from other axiom and aggregates) can provide a
 * specialization of the parse() function.
 *
 * parse() is the function that the user should call. By default, it forward it
 * call to default_parse(). It's why the axiom must implement it.
 *
 * default_parse() should be called when you have an agregate, and you want to
 * access to the parsing of the aggregate while specializating the parse()
 * function.
 */

namespace internal {
    using iterator = std::string::const_iterator;
};
using namespace internal;

struct Axiom{};

/* Generic prototype for the overload set:
 *
 * inline auto parse(const Axiom&, iterator& it, iterator end);
 * inline auto default_parse(const Axiom&, iterator& it, iterator end);
 * template<class Grammar>
 * inline auto parse_as(const Axiom& axiom, iterator& it, iterator end);
 */

// As a user, you should call this function, and it will automatically parse
// the whole string, using all the overload of parse(), default_parse(), and
// parse_as().
template<class Grammar>
inline auto parse(std::string s)
{
    auto it = std::cbegin(s);
    const auto end = std::cend(s);
    const auto axiom = Grammar();
    return parse(axiom, it, end);
}

};
