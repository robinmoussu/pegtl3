#pragma once

#include <string>

namespace grammar {

struct parse_error{
    std::string::const_iterator it;
    const std::string::const_iterator end;
};

};
