#include <cctype>
#include <string>
#include <vector>
#include <variant>
#include <functional>

#include "error.h"
#include "utils.h"
#include "base.h"

namespace grammar {
using namespace internal;

// basic types
struct alpha:                       Axiom{};
struct alphanum:                    Axiom{};
struct blank:                       Axiom{};
struct digit:                       Axiom{};
template <char C> struct character: Axiom{};
template <class T> struct plus:     Axiom{};
template <class T, class LeftPadding, class RightPadding = LeftPadding> struct pad: Axiom{};
template <class... T> struct seq:   Axiom{};
template <class... T> struct sor:   Axiom{};

// aggregates type
struct string: plus<alphanum>{};
struct number: plus<digit>{};

////////////////////////////////////////////////////////////////////////////////

template<class Function>
char parse_one(iterator& it, iterator end, Function f);
inline int default_parse(const digit&, iterator& it, iterator end);
inline char default_parse(const alpha&, iterator& it, iterator end);
inline char default_parse(const alphanum&, iterator& it, iterator end);
inline char default_parse(const blank&, iterator& it, iterator end);
template<char C>
inline char default_parse(const character<C>&, iterator& it, iterator end);

template<class Output, class T>
inline Output parse_as(const plus<T>& axiom, iterator& it, iterator end);
template<class Out, class Head, class... Tail>
static Out try_call(iterator& it, iterator end);

inline int parse(const grammar::number& axiom, iterator& it, iterator end);
inline auto parse(const grammar::string& axiom, iterator& it, iterator end)
    -> decltype(parse_as<std::string>(axiom, it, end));

template <class T>
inline auto default_parse(const plus<T>& axiom, iterator& it, iterator end)
    -> decltype(parse(util::get_instance<T>::value, it, end));
template <class T, class LeftPadding, class RightPadding>
static auto default_parse(const pad<T, LeftPadding, RightPadding>& axiom, iterator& it, iterator end)
    -> decltype(parse(util::get_instance<T>::value, it, end));
template <class... Ts>
static auto default_parse(const seq<Ts...>& axiom, iterator& it, iterator end)
    -> decltype(std::tuple{parse(util::get_instance<Ts>::value, it, end)...});
template <class... Ts>
static auto default_parse(const sor<Ts...>&, iterator& it, iterator end)
    -> typename util::compute_unique_type<
            std::variant,
            decltype(parse(Ts(), it, end)) ...
        >::value;
template<class Axiom, class It, class End>
inline auto parse(const Axiom& axiom, It& it, End end)
    -> decltype(default_parse(util::get_instance<grammar::string>::value, it, end));

////////////////////////////////////////////////////////////////////////////////

// template<class Function>
// char parse_one(iterator& it, iterator end, Function f);
// 
// inline int default_parse(const digit&, iterator& it, iterator end) {
//     return parse_one(it, end, isdigit) - '0';
// };
// 
// inline char default_parse(const alpha&, iterator& it, iterator end) {
//     return parse_one(it, end, isalpha);
// };
// 
// inline char default_parse(const alphanum&, iterator& it, iterator end) {
//     return parse_one(it, end, isalnum);
// };
// 
// inline char default_parse(const blank&, iterator& it, iterator end) {
//     return parse_one(it, end, isblank);
// };
// 
// template<char C>
// inline char default_parse(const character<C>&, iterator& it, iterator end) {
//     return parse_one(it, end, [](char c) {return c == C;});
// }
// 
// template<class Output, class T>
// inline Output parse_as(const plus<T>& axiom, iterator& it, iterator end) {
//     auto first_element = parse(util::get_instance<T>::value, it, end);
//     auto out = Output();
//     out.push_back(first_element); // we could use emplace/push_back based on SFNIAE deduction
// 
//     // ignore parsing error if at least one element was correctly parsed
//     try {
//         // nb: since parse auto-increment the iterator, we don't have to increment it here
//         while (it != end) {
//             out.push_back(parse(util::get_instance<T>::value, it, end));
//         }
//     }
//     catch (parse_error) {}
//     return out;
// }
// 
// template <class T>
// inline auto default_parse(const plus<T>& axiom, iterator& it, iterator end)
//     -> std::vector<decltype(parse(util::get_instance<grammar::number>::value, it, end))>
//     // -> std::vector<decltype(parse(util::get_instance<T>::value, it, end))>
// {
//     return parse_as<std::vector<decltype(parse(util::get_instance<T>::value, it, end))>>(axiom, it, end);
// }
// 
// template <class T, class LeftPadding, class RightPadding>
// static auto default_parse(const pad<T, LeftPadding, RightPadding>& axiom, iterator& it, iterator end)
//     -> decltype(parse(util::get_instance<T>::value, it, end))
// {
//     const auto it_start = it;
//     try {
//         parse(util::get_instance<plus<LeftPadding>>::value, it, end);
//         auto t = parse(util::get_instance<T>::value, it, end);
//         parse(util::get_instance<plus<RightPadding>>::value, it, end);
//         return t;
//     } catch (parse_error&) {
//         // in case of error, rewind the iterator
//         it = it_start;
//         throw;
//     }
// }
// 
// template <class... Ts>
// static auto default_parse(const seq<Ts...>& axiom, iterator& it, iterator end)
//     -> decltype(std::tuple{parse(util::get_instance<Ts>::value, it, end)...})
// {
//     const auto it_start = it;
//     try {
//         return std::tuple{parse(util::get_instance<Ts>::value, it, end)...};
//     } catch (parse_error&) {
//         // in case of error, rewind the iterator
//         it = it_start;
//         throw;
//     }
// }
// 
// template<class Out, class Head, class... Tail>
// static Out try_call(iterator& it, iterator end)
// {
//     try {
//         return parse(util::get_instance<Head>::value, it, end);
//     } catch(parse_error&) {
//         if constexpr(sizeof...(Tail) > 0) {
//             // ignore error, try with the next parameter pack
//             return try_call<Out, Tail...>(it, end);
//         } else {
//             // neither of the parameter was a valid expression
//             throw parse_error{it, end};
//         }
//     }
// };
// 
// template<class...Ts>
// static auto default_parse(const sor<Ts...>&, iterator& it, iterator end)
//     -> typename util::compute_unique_type<
//             std::variant,
//             decltype(parse(Ts(), it, end)) ...
//         >::value
// {
//     const auto it_start = it;
//     try {
//         using ret_type = typename util::compute_unique_type<
//             std::variant,
//             decltype(parse(Ts(), it, end)) ...
//         >::value;
//         return try_call<ret_type, Ts...>(it, end);
//     } catch (parse_error&) {
//         // in case of error, rewind the iterator
//         it = it_start;
//         throw;
//     }
// }
// 
// inline auto parse(const grammar::string& axiom, iterator& it, iterator end)
//     -> decltype(parse_as<std::string>(axiom, it, end))
// {
//     return parse_as<std::string>(axiom, it, end);
// }
// 
// struct number_base_10 {
//     int value;
//     void push_back(int i) {
//         this->value *= 10;
//         this->value += i;
//     }
// };
// inline int parse(const grammar::number& axiom, iterator& it, iterator end) {
//     return parse_as<number_base_10>(axiom, it, end).value;
// }
// 
// // must be defined after all default_parse function are declared
// template<class Axiom, class It, class End>
// inline auto parse(const Axiom& axiom, It& it, End end)
//     -> decltype(default_parse(axiom, it, end))
// {
//     return default_parse(axiom, it, end);
// }

}; // namespace grammar
