#pragma once

#include <variant>

namespace util {

template<class S>
struct to_struct {
    template<class Tup>
    static S get( Tup&&tup ) {
        using T=std::remove_reference_t<Tup>;

        return get(
                std::make_index_sequence<std::tuple_size<T>{}>{},
                std::forward<Tup>(tup)
                );
    }
private:
    template<std::size_t...Is, class Tup>
    static S get( std::index_sequence<Is...>, Tup&& tup ) {
        using std::get;
        return {get<Is>(std::forward<Tup>(tup))...};
    }
};

constexpr bool not_(bool b) {
    return !b;
}

template<class T, class... Ts>
constexpr bool is_new_type() {
    return !(std::is_same<T,Ts>::value || ...);
}

template<class...>
struct pack{};

template<class T, class... Ts>
struct add_type_if_unique;
template<class T, class... Ts>
struct add_type_if_unique<T, pack<Ts...>> {
    using value = typename std::conditional<
        is_new_type<T, Ts...>(),
        pack<Ts..., T>,
        pack<Ts...>>::type;
};
template<class T>
struct add_type_if_unique<T, pack<>> {
    using value = pack<T>;
};

template<class Sorted, class ToSort, class... ToSorts>
struct compute_pack_unique_type{
    using value = typename compute_pack_unique_type<
        typename add_type_if_unique<ToSort, Sorted>::value,
        ToSorts...>::value;
};

template<class Sorted, class ToSort>
struct compute_pack_unique_type<Sorted, ToSort> {
    using value = typename add_type_if_unique<ToSort, Sorted>::value;
};

template<template<class...>class To, class From>
struct unpack_as;
template<template<class...>class To, class... Ts>
struct unpack_as<To, pack<Ts...>> {
    using value = To<Ts...>;
};

template<template<class...>class To, class... Ts>
struct compute_unique_type {
    using value = typename unpack_as<
        To,
        typename compute_pack_unique_type<
            pack<>,
            Ts...
        >::value
    >::value;
};

template<class T>
struct get_instance {
    static inline const T value = T();
};

};

